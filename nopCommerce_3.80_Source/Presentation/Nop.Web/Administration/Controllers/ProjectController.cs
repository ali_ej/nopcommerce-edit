﻿using AutoMapper.QueryableExtensions;
using Nop.Services.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Admin.Models.Project;
using Nop.Core.Domain.Projects;


namespace Nop.Admin.Controllers
{
    public partial class ProjectController : BaseAdminController
    {
        private readonly IProjectService _projectService;
        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        //  [ActionName("Index")]
        // GET: Project
        public ActionResult Index()
        {
            var projects = _projectService.GetAllProjects(show_all: true);
            return View(projects);

        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ProjectModel model)
        {
            var project = new Project()
            {
                ShortDescription = model.ShortDescription,
                FullDescription = model.FullDescription,
                Published = model.Published,
                Name = model.Name
            };
            _projectService.InsertProject(project);
            return RedirectToAction(nameof(Index));
        }

        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var model = _projectService.GetProjectById(id);
            if (model == null)
                return RedirectToAction("Index");
            var project = new ProjectModel()
            {
                Id = model.Id,
                FullDescription = model.FullDescription,
                Name = model.Name,
                ShortDescription = model.ShortDescription,
                Published = model.Published
            };

            return View(project);
        }


        [HttpPost]
        public ActionResult Edit(ProjectModel model)
        {
            var project = new Nop.Core.Domain.Projects.Project()
            {
                Id = model.Id,
                ShortDescription = model.ShortDescription,
                FullDescription = model.FullDescription,
                Published = model.Published,
                Name = model.Name
            };
            _projectService.EditProject(project);
            //return RedirectToAction("Index");
            return RedirectToAction(nameof(Index));
        }

        public ActionResult Delete(int id)
        {
            _projectService.DeleteProject(id);
            return RedirectToAction(nameof(Index));
        }




    }
}