﻿using Nop.Core;
using Nop.Core.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Project
{
    public class ProjectService : IProjectService
    {
        private readonly IRepository<Nop.Core.Domain.Projects.Project> _projectRepository;

        public ProjectService(IRepository<Nop.Core.Domain.Projects.Project> projectRepository)
        {
            _projectRepository = projectRepository;
        }
        public void DeleteProject(int projectId)
        {
            var project = _projectRepository.GetById(projectId);
            _projectRepository.Delete(project);
        }

        public void EditProject(Core.Domain.Projects.Project model)
        {
            var project = _projectRepository.GetById(model.Id);
            project.Name = model.Name;
            project.FullDescription = model.FullDescription;
            project.ShortDescription = model.ShortDescription;
            _projectRepository.Update(project);
            
        }

        public ICollection<Core.Domain.Projects.Project> GetAllProjects(bool show_all=false)
        {
            if (show_all == false) 
            {
               var projects = _projectRepository.Table.Where(p => p.Published == true).ToList();
                return projects;
            }

            var all_projects = _projectRepository.Table.ToList();
            return all_projects;
        }

        public Core.Domain.Projects.Project GetProjectById(int id)
        {
            var project = _projectRepository.GetById(id);
            return project;
        }

        public void InsertProject(Nop.Core.Domain.Projects.Project project)
        {
            _projectRepository.Insert(project);
        }
    }
}
