﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Domain.Projects;

namespace Nop.Services.Project
{
    public interface IProjectService
    {
        Nop.Core.Domain.Projects.Project GetProjectById(int id);
        ICollection<Core.Domain.Projects.Project> GetAllProjects(bool show_all);
        void InsertProject(Nop.Core.Domain.Projects.Project project);
        void EditProject(Nop.Core.Domain.Projects.Project model);
        void DeleteProject(int projectId);
    }
}
