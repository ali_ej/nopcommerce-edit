﻿using Nop.Core.Domain.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Projects
{
    public partial class ProjectMap: NopEntityTypeConfiguration<Project>
    {
        public ProjectMap()
        {
            this.ToTable("Project");
            this.HasKey(c => c.Id);
            this.Property(u => u.Name).IsRequired().HasMaxLength(100);
            this.Property(u => u.ShortDescription).IsRequired().HasMaxLength(500);
            this.Property(u => u.FullDescription).HasMaxLength(2000);

          
        }
    }
}
